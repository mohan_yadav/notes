@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            @include('notes._listing')
        </div>
        <div class="col-md-8">
            @include('notes._form')
        </div>
    </div>
@endsection
