<div class="panel panel-default">
    <div class="panel-heading pageName">
        <h2>NOTES <a class="pull-right btn btn-primary" href="{{ route('add_note') }}"><i class="fa fa-plus"></i> Add New</a></h2>
        <span>
            <small>{{ $notes->count() }} Notes</small>
            <select name="" id="sort_list" class="pull-right form-control">
                @foreach($sortTypes as $key=>$value)
                    <option value="{{ $key }}" {{ ($key == $url) ? 'selected' : '' }}>
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </span>
        <div class="clearfix"></div>
    </div>

    <div class="panel-body list-group no-padding notes-list">
        @foreach($notes as $note)
            <a href="{{ route('edit_note', ['slug'=>$note->slug]) }}" class="list-group-item notes">
                <h4>{{ $note->title }}</h4>
                <div class="small note-date">
                    <i class="fa fa-clock-o"></i>
                    <span> {{ time_elapsed_since($note->updated_at)}}</span>
                </div>
                <p>{{ str_limit(strip_tags($note->note),90,'...') }}</p>
            </a>
        @endforeach
    </div>
</div>

@section('custom-js')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

            $('#sort_list').change(function () {
                url = '{{ url('notes') }}' + $(this).val();
                console.log(url);
                window.location.href = url ;
            });

        });

    </script>
@endsection