<div class="panel panel-default">
    <form action="{{ (isset($noteDetails)) ? route('update_note', ['slug'=>$noteDetails->slug]) :  route('save_note')  }}" id="noteForm" method="post">
        @if(isset($noteDetails))
            <input type="hidden" name="_method" value="PUT">
        @endif
        {{ csrf_field() }}
        <div class="panel-heading note-title-box">
            <div class="row">
                <div class="col-md-10">
                    <input type="text" name="title" id="title" class="note-title" placeholder="Title Your Note" maxlength="50" tabindex="1" autofocus="autofocus" autocomplete="off" value="{{isset($noteDetails->title) ? $noteDetails->title : ''}}">
                </div>
                <div class="col-md-2">
                    @if(isset($noteDetails->slug))
                        <a href="{{ route('delete_note', ['slug'=>$noteDetails->slug]) }}" onclick="return confirm('Are you sure you want to delete this Note ?')" id="deleteNote" class="action" tabindex="4" title="Delete Note">
                            <i class="fa fa-trash-o fa-3x action"></i>
                        </a>
                    @endif
                        <a href="javascript:void(0);" id="saveNote" class="action" tabindex="3" title="Save Note">
                            <i class="fa fa-save fa-3x action"></i>
                        </a>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="note">
                {{--<div id="notea" class="note" contenteditable="true"></div>--}}
                <textarea name="note" id="note" class="note" tabindex="2">
                    {{ (isset($noteDetails->note)) ? $noteDetails->note : '' }}
                </textarea>
            </div>
        </div>
    </form>
</div>

@section('custom-js')
    @parent

    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>

    <script type="text/javascript">
        CKEDITOR.config.height = '19em';
        CKEDITOR.replace('note');


        $('#saveNote').click(function () {
            if($.trim($('#title').val()) == ''){
                alert('Title cannot be blank !')
                return false;
            }
            $('#noteForm').submit();
        });
    </script>
@endsection