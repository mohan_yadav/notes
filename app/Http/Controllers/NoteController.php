<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class NoteController extends Controller {

    protected $data;

    public function __construct(Request $request) {

        parent::__construct();

        $this->data = [
            'sortTypes' => [
                '' => 'Date Updated (newest first)',
                "?ord=desc&sort=updated_at" => 'Date Updated (oldest first)',
                "?ord=asc&sort=updated_at" => 'Date Created (newest first)',
                "?ord=desc&sort=created_at" => 'Date Created (oldest first)',
                "?ord=asc&sort=title" => 'Title (ascending)',
                "?ord=desc&sort=title" => 'Title (descending)'
            ],
            'url' => str_replace($request->url(),'',$request->fullUrl()),
        ];

        $this->middleware(function ($request, $next) {
            $this->data['notes'] = Auth::user()->notes()->orderby((
                $request->get('sort')) ? $request->get('sort') : 'updated_at',
                ($request->get('ord')) ? $request->get('ord') : 'desc'
            )->get();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        //dd($request->user()->notes()->orderby(($request->get('sort')) ? $request->get('sort') : 'updated_at',($request->get('ord')) ? $request->get('ord') : 'desc')->get());
        return view('notes.index', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Note::create([
            'title'=>$request->input('title'),
            'note'=>$request->input('note'),
            'user_id'=> $request->user()->id,
            'slug'=> $this->generate_unique_slug($request->input('title'))
        ]);
        return redirect(route('notes'))->with([
            'status' => 'success',
            'msg' => 'Note Saved Successfully !!'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note) {
        if(Gate::allows('edit-note', $note)){
            $this->data['noteDetails'] = $note;
            return view('notes.index', $this->data);
        }else{
            return redirect(route('notes'))->with([
                'status' => 'danger',
                'msg' => 'You are not Authorized to View this post !!'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        if(Gate::allows('edit-note', $note)){
            $note->update([
                'title'=>$request->input('title'),
                'note'=>$request->input('note'),
            ]);
            return redirect(route('notes'))->with([
                'status'=>'success',
                'msg'=>'Note Updated Successfully !!'
            ]);
        }else{
            return redirect(route('notes'))->with([
                'status' => 'danger',
                'msg' => 'You are not Authorized to Update post !!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        if(Gate::allows('delete-note', $note)){
            $note->delete();
            return redirect(route('notes'))->with([
                'status' => 'success',
                'msg'=>'Note Removed Successfully !!'
            ]);
        }else{
            return redirect(route('notes'))->with([
                'status' => 'danger',
                'msg' => 'You are not Authorized to Delete this post !!'
            ]);
        }
    }

    /**
     * @param string $title
     * @return  string
     */
    private function generate_unique_slug($title){
        $slug = Str::slug($title, '-');
        return (Note::where('slug', $slug)->count()) ? $slug.uniqid() :$slug ;
    }
}
