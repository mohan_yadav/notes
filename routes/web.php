<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.register');
});

Auth::routes();

// Route::get('/home', 'HomeController@index');


Route::group(['prefix' => 'notes', 'middleware' => 'auth'], function () {

    Route::get('/', ['as'=>'notes', 'uses'=>'NoteController@index']);
    Route::get('/add', ['as'=>'add_note', 'uses'=>'NoteController@index']);
    Route::post('/', ['as'=>'save_note','uses' => 'NoteController@store']);
    Route::get('/{slug}', ['as'=>'edit_note','uses' => 'NoteController@edit']);
    Route::put('/{slug}', ['as'=>'update_note','uses' => 'NoteController@update']);
    Route::get('/remove/{slug}', ['as'=>'delete_note','uses' => 'NoteController@destroy']);

});